$(function () {
    $.getJSON("demos.json", function (demos) {
        var ul = $("#list");
        for (var i = 0; i < demos.length; i++) {
            var demo = demos[i],
                li,
                a;

            li = $("<li></li>");

            a = $("<a></a>",{
                href: "/" + demo.key + "/"
            });
            a.text(demo.name);

            li.text(demo.desc ? " - " + demo.desc : "");
            li.prepend(a);

            ul.append(li);
        }
    });
});
